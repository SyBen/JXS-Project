var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(express.static(__dirname + "/public"));

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/public/index.html');
});

http.listen(8080);

console.log('\nServer running at http://127.0.0.1:8080/');
console.log('You can access the config files located in the "/public" folder');

io.on('connection', function (socket) {
    console.log('connection');
    socket.emit('message1', Math.floor((Math.random() * 100) + 1));
    socket.emit('message2', Math.floor((Math.random() * 100) + 1));
    socket.emit('message3', Math.floor((Math.random() * 100) + 1));
    socket.emit('message4', (Math.random() * 100) + '|' + (Math.random() * 100));

});



/*var fs = require('fs');
fs.writeFile('server/public/data.json', "saaaaaaaaave", function (err) {
          if (err) throw err;
          console.log('It\'s saved!');
        });*/


// http://blog.modulus.io/absolute-beginners-guide-to-nodejs