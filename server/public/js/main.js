var socket = io.connect('http://127.0.0.1:8080');
var coordx;
var coordy;
var array;

socket.on('message4', function(message) {
  array = message.split('|');
  var latLng = new google.maps.LatLng(parseFloat(array[0]), parseFloat(array[1]));
  var mapOptions = {
      zoom: 8,
      center: latLng
  };

  var map = new google.maps.Map(document.getElementById('map'), mapOptions);

  
 var marker = new google.maps.Marker({
      position: latLng,
      map: map,
      title: 'bike'
  });
});

socket.on('message1', function(message) {
    console.log(document.getElementById("batteryState").innerHTML = message);
    
    var intMessage = parseInt(message);
    console.log(parseInt(message));

    var string = Fichier('data.json');
    var array = JSON.parse(string);
    var charge = parseInt(array[0].charge); //if charge = 1 : the battery is in charge else it's not

    if(charge == 1){
        window.setInterval("increment(100)", 1000); //incrémenter jusqu'à 100 toutes les secondes
        console.log(document.getElementById("encharge").innerHTML = 'Battery State (Charging.. )');
    }
    else{
        window.setInterval("decrement(0)", 1000); //decrémenter jusqu'à 0 toutes les secondes
        console.log(document.getElementById("encharge").innerHTML = 'Battery State (Decharging.. )');
    }
    
});

socket.on('message2', function(message) {
    //window.localStorage.setItem('maximumSpeed',0);

    //last random speed
    console.log(document.getElementById("LastSpeed").innerHTML = message);
    var lastSpeed = parseInt(message);

    //maximum speed
    
    var max = window.localStorage.getItem('maximumSpeed'); 
    console.log(document.getElementById("MaximumSpeed").innerHTML = max);
    
    if(lastSpeed > max){
        console.log(document.getElementById("MaximumSpeed").innerHTML = lastSpeed);
        //change the maximum speed in json file
        window.localStorage.setItem('maximumSpeed',lastSpeed); 

    }
});

socket.on('message3', function(message) {
    //window.localStorage.setItem('totalDistance',0);

    //last random distance
    console.log(document.getElementById("LastDistance").innerHTML = message);
    var lastDistance = parseInt(message);

    //total distance
    var totalDistance = parseInt(window.localStorage.getItem('totalDistance'));
    totalDistance = totalDistance + lastDistance;
    console.log(document.getElementById("TotalDistance").innerHTML = totalDistance);

    //change the total distance 
     window.localStorage.setItem('totalDistance',totalDistance); 


});

function decrement(min) {

    var h = document.getElementById("batteryState").innerHTML;

    if ( h > min ) {
        h--;
        document.getElementById("progressbar").style.width = h + "%";
        console.log(document.getElementById("batteryState").innerHTML = h);

        if(h == 0) {
            document.getElementById("deadBattery").style.display = "block";
            document.getElementById("lowBattery").style.display = "none";
            document.getElementById("fullBattery").style.display = "none";
        } else if(h <= 10) {
            document.getElementById("lowBattery").style.display = "block";
            document.getElementById("deadBattery").style.display = "none";
            document.getElementById("fullBattery").style.display = "none";
        } else if (h == 100) {
            document.getElementById("fullBattery").style.display = "block";
            document.getElementById("lowBattery").style.display = "none";
            document.getElementById("deadBattery").style.display = "none";
        } else {
            alerts = document.querySelectorAll(".alert");
            console.log(alerts);
            for(var i = 0;i < alerts.length;i++) {
                alerts[i].style.display = "none"
;            }
        }
    } 
}

function increment(max) {

    var h = document.getElementById("batteryState").innerHTML;

    if ( h < max ) {
        h++;
        document.getElementById("progressbar").style.width = h + "%";
        console.log(document.getElementById("batteryState").innerHTML = h);

        if(h == 0) {
            document.getElementById("deadBattery").style.display = "block";
            document.getElementById("lowBattery").style.display = "none";
            document.getElementById("fullBattery").style.display = "none";
        } else if(h <= 10) {
            document.getElementById("lowBattery").style.display = "block";
            document.getElementById("deadBattery").style.display = "none";
            document.getElementById("fullBattery").style.display = "none";
        } else if (h == 100) {
            document.getElementById("fullBattery").style.display = "block";
            document.getElementById("lowBattery").style.display = "none";
            document.getElementById("deadBattery").style.display = "none";
        } else {
            alerts = document.querySelectorAll(".alert");
            console.log(alerts);
            for(var i = 0;i < alerts.length;i++) {
            alerts[i].style.display = "none";
        }
    }
  }
}

var Fichier = function Fichier(fichier)
{
    if(window.XMLHttpRequest) obj = new XMLHttpRequest(); //Pour Firefox, Opera,...

    else if(window.ActiveXObject) obj = new ActiveXObject("Microsoft.XMLHTTP"); //Pour Internet Explorer 

    else return(false);
    

    if (obj.overrideMimeType) obj.overrideMimeType("text/xml"); //Évite un bug de Safari

   
    obj.open("GET", fichier, false);
    obj.send(null);
   
    if(obj.readyState == 4) return(obj.responseText);
    else return(false);
}